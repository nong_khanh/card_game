var getNames = document.querySelector('.names');
var getInfo = document.querySelector('.info');
var getSelect = document.querySelector('.select');
var getSelectItems = document.querySelectorAll('.select-items');
var getSelectMobile = document.querySelector('.select-mobile');
var getSelectItemsMobile = document.querySelectorAll('.select-items-mobile');
var getCardImgLove = document.querySelector('.card--img__love');
var getCardImgFriend = document.querySelector('.card--img__friend');
var getCardImgContent = document.querySelector('.card--img__content');
var getStartBtn = document.querySelector('.btn-start');
var getBackBtn = document.querySelector('.back');
var getBackMobileBtn = document.querySelector('.back-mobile');
var getNextBtn = document.querySelector('.btn-next');
var getContinueBtnLove = document.querySelector('.noti__love .btn-continue');
var getContinueBtnFriend = document.querySelector('.noti__friend .btn-continue');
var getNotiLove = document.querySelector('.noti__love');
var getNotiFriend = document.querySelector('.noti__friend');

getNames.innerHTML = `${localStorage.getItem('myName')} và ${localStorage.getItem('partnerName')}`; // in ra ten da duoc nhap tu localStorage
var loveQuestions = [
    'Nếu một ngày cả hai đã cảm thấy mệt mỏi thì bạn và đối phương sẽ duy trì mối quan hệ bằng cách nào?',
    'Bạn cảm thấy không hài lòng nhất ở điểm gì của đối phương?',
    'Tin nhắn/câu nói tình cảm nhất bạn từng nói với người cũ?',
    // 'Kỉ niệm nào của hai người mà bạn không thể quên?',
    // 'Đã bao giờ bạn cảm thấy nên chấm dứt mối quan hệ chưa?',
    // 'Trước khi bắt đầu mối quan hệ bạn đã có bao nhiêu người cũ?',
    // 'Một điều bạn muốn đối phương thay đổi?',
    // 'Điều gì khiến bạn cảm thấy hồi hận vì đã nói/làm với đối phương?',
    // 'Một địa điểm mà hai bạn muốn đi cùng nhau?',
    // 'Lần cãi nhau to nhất của hai bạn là vì lí do gì?',
    // 'Bạn có ấn tượng gì khi gặp đối phương lần đầu?',
    // 'Câu nói/hành động nào của đối phương khiến bạn tổn thương nhất?',
    // 'Bạn muốn được đối phương an ủi như thế nào lúc buồn?',
    // 'Sự kiện/hành động nào của đối phương khiến bạn cảm thấy thiếu an toàn?',
    // 'Có bao giờ bạn bè của họ khiến bạn thấy không thoải mái không?',
    // 'Món quà đối phương tặng mà bạn thích nhất?',
    // 'Nếu đối phương và bạn bè của bạn xích mích bạn sẽ làm gì?',
    // 'Món ăn mà đối phương thích ăn nhất?',
    // 'Các bạn có dự định hôn nhân không?',
    // 'Mối tình lâu dài nhất của bạn là trong bao lâu?',
    // 'Bạn có từng làm gì có lỗi sau lưng người ấy chưa?',
    // 'Một điều mà người ấy đã từng làm khiến bạn cảm thấy hạnh phúc?',
    // 'Khoảng thời gian nào của hai người là khoảng thời gian tồi tệ nhất?', 
    // 'Nếu được quay trở lại quá khứ bạn muốn thay đổi điều gì nhất?',
    // 'Những dự định/kế hoạch của bạn trong tương lai với người ấy?',
    // 'Gia đình của bạn/người ấy có ủng hộ mối quan hệ này không?',
    // 'Đã bao giờ bạn cảm thấy hối hận vì bắt đầu mối quan hệ chưa?',
    // 'Suy nghĩ, cảm nhận của người ấy về bạn?',
    // 'Bạn/người ấy đã từng có ý định không công khai mối quan hệ không?',
    // 'Hai bạn đã quen nhau như thế nào?', 
    // 'Bạn có phải mối tình đầu của người ấy không?',
    // 'Hỏi người ấy về điều xấu hổ nhất mà người ấy đã trải qua?',
    // 'Sao người ấy và người cũ lại chia tay?',
    // 'Người ấy còn cảm giác với ai trong quá khứ không?',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
    // '',
];
var friendQuestions = [
    'Khi mới quen nhau cậu có nghĩ mình sẽ trở thành bạn thân của nhau không?',
    'Kỉ niệm đẹp nhất của hai đứa là gì?',
    'Cậu đã bao giờ nghĩ mình sẽ không thể chơi với nhau tiếp không?',
    'Điều gì mình đã làm khiến cậu thấy vui vẻ nhất?',
    'Cậu muốn bọn mình làm gì/đi đâu cùng nhau nhất', 
    'Sáu',
    'Bảy',
    'Tám',
    'Chín', 
    'Mười',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
];

getStartBtn.onclick = function() {
    blurAnimation(getInfo, '0.25s');
    appearFlexAnimation(getSelect, '0.25s');
    appearFlexAnimation(getSelectMobile, '0.25s');
}

for(let i = 0; i < getSelectItems.length; i++) {
    getSelectItems[i].onclick = function(e){
        blurAnimation(getSelect, '0.25s');
        if(e.target.innerText === 'Tình yêu') {
            sessionStorage.setItem('status', 'love');
            appearInlineBlockAnimation(getCardImgLove, '0.4s');
            getBackBtn.style.display = 'inline-block';
            checkArrEmty(loveQuestions);
        } else {
            sessionStorage.setItem('status', 'friend');
            appearInlineBlockAnimation(getCardImgFriend, '0.4s');
            getBackBtn.style.display = 'inline-block';
            checkArrEmty(friendQuestions);
        }
    }
}


for(let i = 0; i < getSelectItemsMobile.length; i++) {
    getSelectItemsMobile[i].onclick = function(e){
        blurAnimation(getSelectMobile, '0.25s');
        if(e.target.innerText === 'Tình yêu') {
            sessionStorage.setItem('status', 'love');
            appearInlineBlockAnimation(getCardImgLove, '0.4s');
            getBackMobileBtn.style.display = 'inline-block';
            checkArrEmty(loveQuestions);
        } else {
            sessionStorage.setItem('status', 'friend');
            appearInlineBlockAnimation(getCardImgFriend, '0.4s');
            getBackMobileBtn.style.display = 'inline-block';
            checkArrEmty(friendQuestions);
        }
    }
}

getBackBtn.onclick = function() {
    appearFlexAnimation(getSelect, '0.25s');
    blurAnimation(getCardImgLove, '0.4s');
    blurAnimation(getCardImgFriend, '0.4s');
    blurAnimation(getCardImgContent, '0.4s');
    blurAnimation(getNextBtn, '0.25s');
    if(getSelect.style.display == 'flex') {
        getBackBtn.style.display = 'none';
    }
}

getBackMobileBtn.onclick = function() {
    appearFlexAnimation(getSelectMobile, '0.25s');
    blurAnimation(getCardImgLove, '0.4s');
    blurAnimation(getCardImgFriend, '0.4s');
    blurAnimation(getCardImgContent, '0.4s');
    blurAnimation(getNextBtn, '0.25s');
    if(getSelectMobile.style.display == 'flex') {
        getBackMobileBtn.style.display = 'none'; 
    }
}

getCardImgLove.onclick = function() { 
    let randomNumber = Math.floor(Math.random() * loveQuestions.length); // tao so ngau nhien 
    getCardImgContent.innerHTML = loveQuestions[randomNumber];
    deleteElement(loveQuestions, randomNumber); 
    getCardImgLove.style.animation = "spin 1s linear, blur 0.5s linear 1s";
    setTimeout(function(){
        getCardImgLove.style.display = 'none';
        getCardImgContent.style.backgroundColor = '#e66767';
        appearInlineBlockAnimation(getCardImgContent, '0.25s');
        appearBlockAnimation(getNextBtn, '0.25s');
    },700)
}

getCardImgFriend.onclick = function() {
    let randomNumber = Math.floor(Math.random() * friendQuestions.length); // tao so ngau nhien 
    getCardImgContent.innerHTML = friendQuestions[randomNumber];
    deleteElement(friendQuestions, randomNumber); 
    getCardImgFriend.style.animation = "spin 1s linear, blur 0.5s linear 1s";
    setTimeout(function(){
        getCardImgFriend.style.display = 'none';
        getCardImgContent.style.backgroundColor = '#f5cd79';
        appearInlineBlockAnimation(getCardImgContent, '0.25s');
        appearBlockAnimation(getNextBtn, '0.25s');
    },700)
}


getNextBtn.onclick = function() {
    blurAnimation(getNextBtn, '0.25s');
    if(sessionStorage.getItem('status') === 'love') {
        getCardImgContent.style.animation = "spin 1s linear, blur 0.5s linear 1s";
        setTimeout(function(){
            getCardImgContent.style.display = 'none';
            appearInlineBlockAnimation(getCardImgLove, '0.25s');
        },700);
        checkArrQuestions(loveQuestions); // khi da xem het so cau hoi se dua ra thong bao
    } 
    if(sessionStorage.getItem('status') === 'friend') {
        getCardImgContent.style.animation = "spin 1s linear, blur 0.5s linear 1s";        
        setTimeout(function(){
            getCardImgContent.style.display = 'none';
            appearInlineBlockAnimation(getCardImgFriend, '0.25s');
        },700);
        checkArrQuestions(friendQuestions); // khi da xem het so cau hoi se dua ra thong bao
    }
}

getContinueBtnLove.onclick = function() {
    appearFlexAnimation(getSelect, '0.25s');
    appearFlexAnimation(getSelectMobile, '0.25s');
    blurAnimation(getNotiLove, '0.4s');
}

getContinueBtnFriend.onclick = function() {
    appearFlexAnimation(getSelect, '0.25s');
    appearFlexAnimation(getSelectMobile, '0.25s');
    blurAnimation(getNotiFriend, '0.4s');
}

// function
function blurAnimation(htmlDom, seconds) { // ham lam mo` va bien mat di htmlDom
    return Object.assign(htmlDom.style, {
        display: "none",
        animation: `blur ${seconds} linear`
    })
}

function appearBlockAnimation(htmlDom, seconds) { // ham hien thi htmlDom theo display Block
    return Object.assign(htmlDom.style, {
        display: "block",
        animation: `see ${seconds} linear`
    })
}

function appearInlineBlockAnimation(htmlDom, seconds) { // ham hien thi htmlDom theo display inline-block
    return Object.assign(htmlDom.style, {
        display: "inline-block",
        animation: `see ${seconds} linear`
    })
}

function appearFlexAnimation(htmlDom, seconds) { // ham hien thi htmlDom theo display inline-block
    return Object.assign(htmlDom.style, {
        display: "flex",
        animation: `see ${seconds} linear`
    })
}

function deleteElement(arr, number) { // bo di cau hoi trong mang da duoc random ra
    arr.splice(number, 1);
    return arr;
}

function checkArrQuestions(arr) { // khi het phan tu trong mang se dua ra thong bao
    if(arr.length == 0) {  
        blurAnimation(getBackBtn, '0.4s');
        blurAnimation(getBackMobileBtn, '0.4s');
        blurAnimation(getCardImgContent, '0.4s');
        blurAnimation(getNextBtn, '0.4s');
        if(sessionStorage.getItem('status') === 'love') {
            appearBlockAnimation(getNotiLove, '0.4s')
            setTimeout(function(){
                getCardImgLove.style.display = "none";
            }, 700)
        } 
        if(sessionStorage.getItem('status') === 'friend') {
            appearBlockAnimation(getNotiFriend, '0.4s');
            setTimeout(function(){
                getCardImgFriend.style.display = "none";
            }, 700)
        }
    }
}

function checkArrEmty(arr) { // khi het phan tu trong mang se dua ra thong bao
    if(arr.length == 0) {  
        blurAnimation(getBackBtn, '0.4s');
        blurAnimation(getBackMobileBtn, '0.4s');
        blurAnimation(getCardImgContent, '0.4s');
        blurAnimation(getNextBtn, '0.4s');
        if(sessionStorage.getItem('status') === 'love') {
            appearBlockAnimation(getNotiLove, '0.4s')
            getCardImgLove.style.display = "none";
        } 
        if(sessionStorage.getItem('status') === 'friend') {
            appearBlockAnimation(getNotiFriend, '0.4s');
            getCardImgFriend.style.display = "none";
        }
    }
}

// thay doi mau sac cua button
setInterval(() => {
    getContinueBtnLove.classList.toggle('change-color-love');
    getContinueBtnFriend.classList.toggle('change-color-friend');
}, 1000);

